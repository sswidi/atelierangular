import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferemploiComponent } from './offeremploi.component';

describe('OfferemploiComponent', () => {
  let component: OfferemploiComponent;
  let fixture: ComponentFixture<OfferemploiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferemploiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferemploiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
